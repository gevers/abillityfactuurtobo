<?php
	include_once 'obj_Import.php'; 
	function isCommandLineInterface()
	{
		return (php_sapi_name() === 'cli');
	}	
	
	if (isCommandLineInterface())
	{
		foreach ($argv as $arg) {
				$e=explode("=",$arg);
				if(count($e)==2)
						$_GET[$e[0]]=$e[1];
				else    
						$_GET[$e[0]]=0;
		}
	}
	
	if (isset($_GET["insert"]) and ($_GET["insert"] == 1))
	{
		if (isset($_GET["InvoiceDate"]) and validateDate($_GET["InvoiceDate"])){
			$InvLst = new InvoiceLst();
			$InvLst->SetDatumFactuur($_GET["InvoiceDate"]);	
			
			$InvLst->SetAantalFacturenPerDatum($InvLst->GetDatumFactuur());
			if ($InvLst->GetAantalFacturenPerDatum() > 0 ){
				die("Er zijn " .$InvLst->GetAantalFacturenPerDatum(). " facturen met deze invoiceDate gevonden. <br /> De DateInvoice lijkt niet juist. Kies een andere datum!");
			}
		}
		else{
			die("Je hebt een verkeerde datum opgegeven. Datum formaat moet zijn 'yyyy-mm-dd'");
		}
	}
	else{
		die("insert not ok");
	}
	
	function validateDate($date, $format = 'Y-m-d')
	{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
	}	


	function ImportCSV2Array($filename)
	{
		$row = 0;
		$col = 0;
		$handle = @fopen($filename, "rb");
		if ($handle) 
		{
			while (($row = fgetcsv($handle, 4096, ";")) !== false) 
			{
				if (empty($fields)) 
				{
						$fields = $row;
						continue;
				}
				foreach ($row as $k=>$value) 
				{
					$results[$col][$fields[$k]] = $value;
					$results[$col][$k] = $value;
				}
				$col++;
				unset($row);
			}
			if (!feof($handle)) 
			{
				die("Error: unexpected fgets() failn");
			}
			fclose($handle);
		}
		return $results;
	}
		
	$InvLst->CreateLog();
	$InvLst->StartDateTime = date("d-m-Y H:i:s", time());
	$filename = "C:/xampp/htdocs/maandfactuur/doc/maintype.csv";
	$csvArray = ImportCSV2Array($filename);
	$InvLst->SetInvoiceCount(sizeof($csvArray));
	echo "<table border=1 cellpadding=5 cellspacing=0>";
	echo "<tr>";
	echo "<td>RegelNummer</td>";
	echo "<td>CustomerNumber</td>";
	echo "<td>InvoiceNo</td>";
	echo "<td>Invoice total (inc VAT)</td>";
	echo "<td>Vat Amount 21.00</td>";
	echo "<td>Invoice total (exc VAT)</td>";
	echo "<td>Inserted in DB</td>";
	echo "<td>PDF aanwezig</td>";	
	echo "</tr>";
		usort($csvArray, function($a, $b) {
		 if (isset($a[0]) && isset($b[0])){
				return $a[0] - $b[0];
		 }else {
			 foreach ($a as $i=>$v){
				 echo "\n'$i' => '$v'";
			 }
			 die ("\n\na = " . print_r($a,true) . "\nb = ". print_r($b,true));
		 }
		});
	$now=time();
	file_put_contents("C:/xampp/htdocs/maandfactuur/doc/".'info.inc.php', "<?php\n\$start={$now};\n\$invoiceCount={$InvLst->InvoiceCount};\n\$FactuurDatum='{$InvLst->DatumFactuur}';");

	foreach ($csvArray as $id=>$row)
	{	
			$InvLst->setKlantID($row[0]);
			$InvLst->regelnummer = $id + 1;
			$InvLst->FactuurNr = $row['InvoiceNo'];
			$tmp = $row['Invoice total (inc VAT)'];
			$InvLst->TotaalInc = str_replace(',', '.', $row['Invoice total (inc VAT)']);
			$InvLst->TotaalBTW = str_replace(',', '.', $row['Vat Amount 21.00']);
			$InvLst->TotaalEx = str_replace(',', '.', $row['Invoice total (exc VAT)']);		
			$factuur = "C:/xampp/htdocs/maandfactuur/doc/PDF/" .$InvLst->FactuurNr. ".pdf";
			if (file_exists($factuur)){
				$InvLst->setFoundPdf(1);
				$InvLst->setPdf($factuur);
			} else {
				$InvLst->setFoundPdf(0);
			}
			$InvLst->saveToDb();

			echo "</td>";
			echo "</tr>";
			echo "\n";			
	}		
	echo "</table>";
	$InvLst->SetAantalFactImported();
	$InvLst->EndDateTime = date("d-m-Y H:i:s", time());
	$InvLst->EmailLog();
	
?>