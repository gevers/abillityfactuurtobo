<!DOCTYPE html>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script>
		
		function updateStatus()
		{
				$("#voortgang").load("count.php?"+Math.random(), function(responseTxt, statusTxt, xhr){
			if(statusTxt == "error")
				$("#voortgang").html(xhr.status + ": " + xhr.statusText); 
			});
			if ($("#voortgang").text() != "Voltooid!"){
				setTimeout(updateStatus, 1500);
			}
		}
		
		
		$(document).ready(function(){

				$("#bgjob").click(function(){
						$("#div1").load("ImportFactuurBG.php?insert=1&InvoiceDate=" + $("#datepicker").val() + "", function(responseTxt, statusTxt, xhr){
								if(statusTxt == "error")
									$("#div1").html(xhr.status + ": " + xhr.statusText); 
						});
						updateStatus();
				});
		});
		
		$(function() {
				$( "#datepicker" ).datepicker();
				$( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
				$( "#datepicker" ).datepicker( "", "dateFormat", "yy-mm-dd" );
			});

		</script>
	</head>
	<body>
		<p>Factuurdatum: <input type="text" id="datepicker"></p>
		<button id="bgjob" value>Start import on background</button>			
		<br />
		<br />
		<a href="doc/log/log.html" tartget="_new">Voorgang bekijken</a>
		
		
		<p>
			<div id="div1"></div>
			<div id="voortgang"></div>
		</p>
	</body>
</html>