<?php
	class InvoiceLst{
		public $AccountingNumber;
		public $KlantID;
		public $FactuurNr;
		public $TotaalEx;
		public $TotaalBTW;
		public $TotaalInc;
		public $DatumFactuur;
		public $Pdf;
		public $NewInsertID;
		public $logfile;
		public $conn;
		public $FoundPdf;
		private $DbInsertSucces;
		public $AantalFacturenPerDatum;
		
		public function __construct() { 
			$this->conn_array = array (
						"UID" => "ks-gerard",
						"PWD" => "SoTgRLi6LQR?OV!P2DVO",
						"Database" => "KliksafeStore_Prep",
			);
			$this->conn = sqlsrv_connect('89.146.41.114', $this->conn_array );			
			if(!$this->conn) {
				die( print_r( sqlsrv_errors(), true));
			}
			$this->CreateLog();
		}
	
		public function SetAantalFacturenPerDatum($InvoiceDate){
			$sql = "SELECT COUNT(*) as Aantal
							FROM InvoiceArchiveLst
							WHERE DateInvoice = '".$InvoiceDate. "'";
							
			$result = sqlsrv_query($this->conn, $sql);
			if($result){  
				$row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC);
				$this->AantalFacturenPerDatum = $row['Aantal'];
			}
			else{  
				die( print_r( sqlsrv_errors(), true));
			} 
			sqlsrv_free_stmt($result);								
		}
	
		public function GetAantalFacturenPerDatum(){
			return $this->AantalFacturenPerDatum;
		}
		
		public function __destruct(){
			fclose($this->logfile);
		}
		
		public function SetDatumFactuur($Datum){
			$this->DatumFactuur = $Datum; 
		}
		
		public function GetDatumFactuur(){
			return $this->DatumFactuur;
		}
		
		public function setFoundPdf($found){
			$this->FoundPdf = $found; 	
		}
		
		public function setPdf($aPdf){
			$handle = @fopen($aPdf, 'rb');
			if ($handle)
			{
				$content = @fread($handle, filesize($aPdf));
				$content = bin2hex($content);
				@fclose($handle);
				$this->Pdf = "0x".$content;
			}	
		}
		
		private function CreateLog(){
			$filename = date("Ymd_His")."_SavePdfLog";
			$this->logfile = fopen("doc/log/".$filename.".csv", "w");
			fputcsv($this->logfile, array('KlantID','FactuurNr', 'TotaalInc', 'TotaalEx', 'TotaalBTW', 'InvoiceID', 'DatumFactuur', 'DatumExport', 'TijdExport', 'Opgeslagen in DB', 'PDF aanwezig'), ";");
		}
		
		public function setNewInsertID() {
			$sql = "select Max(ID) as ID FROM InvoiceArchiveLst";
			$result = sqlsrv_query($this->conn, $sql);

			if($result){  
				$row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC);
				$this->NewInsertID = $row['ID'] + 1;
			}
			else{  
				die( print_r( sqlsrv_errors(), true));
			} 
			sqlsrv_free_stmt( $result);
		}	

    public function ResetObject() {
			foreach ($this as $key => $value) {
					unset($this->$key);
			}
    }		

		public function setKlantID($aID)
		{
			$sql = "SELECT org.ID as ID
							FROM Organizations org
							WHERE org.AccountingNumber = ".$aID;
			
			$result = sqlsrv_query($this->conn, $sql);
			if($result){  
				$row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC);
				$this->KlantID = $row['ID'];
			}
			else{  
				die( print_r( sqlsrv_errors(), true));
			} 
			sqlsrv_free_stmt($result);	
		}
		
		public function saveToDb($save_obj)
		{
			$klantID = $save_obj->KlantID;
			$this->setKlantID($klantID);
			$this->setNewInsertID();
			
							
			$FactuurNr = $save_obj->FactuurNr;
			$TotaalEx = $save_obj->TotaalEx;
			$TotaalBTW = $save_obj->TotaalBTW;
			$TotaalInc = $save_obj->TotaalInc;
			$Pdf = $save_obj->Pdf;
			$FoundPdf = $save_obj->FoundPdf;
			$this->DbInsertSucces = true;

		if($FoundPdf){ //alleen als er een PDF aanwezig is wordt er een regel in de BO db weggeschreven

			 //transation start
			if ( sqlsrv_begin_transaction( $this->conn ) === false ) {
				 die( print_r( sqlsrv_errors(), true ));
			}

			$queryLst = "INSERT INTO [dbo].[InvoiceArchiveLst]
						 ([ID]
						 ,[InvoiceType]
						 ,[OrganizationID]
						 ,[DateCreated]
						 ,[DateInvoice]
						 ,[DateSended]
						 ,[DateExported]
						 ,[TotalCostSale]
						 ,[TotalGross]
						 ,[TotalVat]
						 ,[TotalInvoice])
			 VALUES
						 (".$this->NewInsertID."
						 ,'MM'
						 ,".$this->KlantID."
						 ,".date("'Y-m-d H:i:s'")."
						 ,'".$this->DatumFactuur."'
						 ,".date("'Y-m-d H:i:s'")."
						 ,".date("'Y-m-d H:i:s'")."
						 ,".$this->TotaalEx."
						 ,".$this->TotaalEx."
						 ,".$this->TotaalBTW."
						 ,".$this->TotaalInc.")";
						 
				$resultLst = sqlsrv_query($this->conn, $queryLst);

				$queryPdf = "INSERT INTO InvoiceArchivePdf
						 ([InvoiceArchiveLstID]
						 ,[PdfText])
				VALUES
							(".$this->NewInsertID.",
						 ".$this->Pdf.")";		

				$resultPdf = sqlsrv_query($this->conn, $queryPdf);	

				//transaction end
				if( $resultLst && $resultPdf) {
					sqlsrv_commit( $this->conn );
					$this->DbInsertSucces = 'ja';
				} else {
						sqlsrv_rollback( $this->conn );
						$this->DbInsertSucces = 'nee';
				}
		}


			echo "<tr>";
			echo "<td>";
			echo $this->KlantID;
			echo "</td>";			
			echo "<td>";
			echo $this->FactuurNr;
			echo "</td>";			
			echo "<td>";
			echo $this->TotaalInc;
			echo "</td>";			
			echo "<td>";
			echo $this->TotaalBTW;
			echo "</td>";			
			echo "<td>";
			echo $this->TotaalEx;
			echo "</td>";			
			echo "<td>";
			if ($this->DbInsertSucces) {
				echo "ja";
			}
			else {
				echo "nee";
			}
			echo "</td>";			
			echo "<td>";
			if ($this->FoundPdf) {
				echo "ja";
			}
			else {
				echo "nee";
			}


		$this->writeLogRegel($this->DbInsertSucces);			
		}
		
		function writeLogRegel()
		{
			fputcsv($this->logfile, array($this->KlantID, 
																		$this->FactuurNr, 
																		$this->TotaalInc, 
																		$this->TotaalEx, 
																		$this->TotaalBTW,
																		$this->NewInsertID,
																		$this->DatumFactuur,
																		date("d-m-Y"),
																		date("H:i:s"),
																		$this->DbInsertSucces,
																		$this->FoundPdf
																		), ";");
		}			
	}	
?>