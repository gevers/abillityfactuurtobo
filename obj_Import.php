<?php
	class InvoiceLst{
		public $AccountingNumber;
		public $KlantID;
		public $FactuurNr;
		public $TotaalEx;
		public $TotaalBTW;
		public $TotaalInc;
		public $DatumFactuur;
		public $Pdf;
		public $logfile;
		private $LogFileName;
		private $LogPath;
		public $conn;
		public $FoundPdf;
		public $AantalFacturenPerDatum;
		private $FactuurExistInDB;
		private $DbInsertSucces;
		private $LogReden;
		public $AantalFactImported;
		public $InvoiceCount;
		public $StartDateTime;
		public $EndDateTime;
		public $regelnummer;
		
		public function __construct() {

			$this->conn_array = array (
						"UID" => "ks-gerard",
						"PWD" => "SoTgRLi6LQR?OV!P2DVO",
						"Database" => "KliksafeStore",
			);			
			$this->conn = sqlsrv_connect('212.121.123.58', $this->conn_array );						
			if(!$this->conn) {
				die("$sql geeft: " . (print_r( sqlsrv_errors(), true)));
			}
		}
	
		public function SetAantalFacturenPerDatum($InvoiceDate){
			$sql = "SELECT COUNT(*) as Aantal
							FROM InvoiceArchiveLst
							WHERE DateInvoice = '".$InvoiceDate. "'
							AND invoiceType = 'AM'";
							
			$result = sqlsrv_query($this->conn, $sql);
			if($result){  
				$row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC);
				$this->AantalFacturenPerDatum = $row['Aantal'];
			}
			else{  
				die("$sql geeft: " .  (print_r( sqlsrv_errors(), true)));
			} 
			sqlsrv_free_stmt($result);								
		}
	
		public function GetAantalFacturenPerDatum(){
			return $this->AantalFacturenPerDatum;
		}
		
		public function __destruct(){
			if (file_exists($this->LogFileName)){
				fclose($this->logfile);
			}
		}
		
		public function SetInvoiceCount($aantal){
			$this->InvoiceCount = $aantal;
		}
		public function SetDatumFactuur($Datum){
			$this->DatumFactuur = $Datum; 
		}
		
		public function GetDatumFactuur(){
			return $this->DatumFactuur;
		}
		
		public function setFoundPdf($found){
			$this->FoundPdf = $found; 	
		}
		
		public function setPdf($aPdf){
			$handle = @fopen($aPdf, 'rb');
			if ($handle)
			{
				$content = @fread($handle, filesize($aPdf));
				$content = bin2hex($content);
				@fclose($handle);
				$this->Pdf = "0x".$content;
			}	
		}
		
		public function CreateLog(){
			$filename = date("Ymd_His")."_SavePdfLog";
			$this->LogPath = 'C:/xampp/htdocs/maandfactuur/doc/log/';
			$this->LogFileName = "C:/xampp/htdocs/maandfactuur/doc/log/".$filename.".csv";
			$this->logfile = fopen($this->LogFileName, "w");
			fputcsv($this->logfile, array('KlantID','FactuurNr', 'TotaalInc', 'TotaalEx', 'TotaalBTW', 'InvoiceID', 'DatumFactuur', 'DatumExport', 'TijdExport', 'Opgeslagen in DB', 'PDF aanwezig', 'Reden'), ";");
		}


		private function setLogReden()
		{
			$this->LogReden = "";
			if ($this->FoundPdf == 0){
				$this->LogReden = "Geen PDF bestand gevonden: " .$this->FactuurNr.".PDF";
			} else if ($this->FactuurExist == 1){
				$this->LogReden = "Factuur bestaat al in DB: " .$this->KlantID.". Factuurnr: ".$this->FactuurNr;
			}else if ($this->DbInsertSucces == 0){
				$this->LogReden = "Kan record niet opslaan door onbekende reden. KlantID: " .$this->KlantID.". Factuurnr: ".$this->FactuurNr;
			} 
		}

		//controle of er al een FactuurNr in de database aanwezig is.
		private function setFactuurExistInDB() {
			$sql = "select ID FROM InvoiceArchiveLst WHERE ID = ".$this->FactuurNr;
			$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$result = sqlsrv_query($this->conn, $sql,array(),$options) or die( print_r( sqlsrv_errors(), true));

			$row_count = sqlsrv_num_rows($result);
			if($result)  
			{
				if($row_count > 0){
					$this->FactuurExist = 1;
				}
				else{
					$this->FactuurExist = 0;
				}
			}else{
				die( "$sql geeft: " . (print_r( sqlsrv_errors(), true)));
			}
			sqlsrv_free_stmt( $result);
		}
		
		//functie voor het omzetten van Debiteurnummer naar OrgID.
		public function setKlantID($aID)
		{
			$sql = "SELECT org.ID as ID
							FROM Organizations org
							WHERE org.AccountingNumber = ".$aID;
			$result = sqlsrv_query($this->conn, $sql);
			if($result){  
				$row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC);
				$this->KlantID = $row['ID'];
			}
			else{  
				die( "$sql geeft: " . (print_r( sqlsrv_errors(), true)));
			} 
			sqlsrv_free_stmt($result);	
			
		}
		
		public function saveToDb()
		{
			$this->setFactuurExistInDB();
			if($this->FoundPdf and $this->FactuurExist == 0) //alleen als er een PDF aanwezig EN het factuurnummer niet aanwezig is in de DB, wordt er een regel in de BO db weggeschreven
			{
				 //transation start
				if ( sqlsrv_begin_transaction( $this->conn ) === false ) {
					 die( print_r( sqlsrv_errors(), true ));
				}
				$tmpDate = date("'Y-m-d H:i:s'");

				$queryLst = "INSERT INTO [dbo].[InvoiceArchiveLst]
							 ([ID]
							 ,[InvoiceType]
							 ,[OrganizationID]
							 ,[DateCreated]
							 ,[DateInvoice]
							 ,[DateSended]
							 ,[DateExported]
							 ,[TotalCostSale]
							 ,[TotalGross]
							 ,[TotalVat]
							 ,[TotalInvoice])
				 VALUES
							 ({$this->FactuurNr}
							 ,'AM'
							 ,{$this->KlantID}
							 ,{$tmpDate}
							 ,'{$this->DatumFactuur}'
							 ,{$tmpDate}
							 ,{$tmpDate}
							 ,{$this->TotaalEx}
							 ,{$this->TotaalEx}
							 ,{$this->TotaalBTW}
							 ,{$this->TotaalInc})";
							 
				
				$resultLst = sqlsrv_query($this->conn, $queryLst);
				
				$queryPdf = "INSERT INTO InvoiceArchivePdf
						 ([InvoiceArchiveLstID]
						 ,[PdfText])
				VALUES
							({$this->FactuurNr}
							,{$this->Pdf})";		

				$resultPdf = sqlsrv_query($this->conn, $queryPdf);

				//transaction end
				if( $resultLst && $resultPdf) {;
					sqlsrv_commit( $this->conn );
					$this->DbInsertSucces = 1;
				} else {
						sqlsrv_rollback( $this->conn );
						$this->DbInsertSucces = 0;
				}
			}
			else
			{
				$this->DbInsertSucces = 0;
			}

			echo "<tr>";
			echo "<td>";
			echo $this->regelnummer . "/{$this->InvoiceCount}";
			echo "</td>";			
			echo "<td>";
			echo $this->KlantID;
			echo "</td>";			
			echo "<td>";
			echo $this->FactuurNr;
			echo "</td>";			
			echo "<td>";
			echo $this->TotaalInc;
			echo "</td>";			
			echo "<td>";
			echo $this->TotaalBTW;
			echo "</td>";			
			echo "<td>";
			echo $this->TotaalEx;
			echo "</td>";			
			echo "<td>";
			if ($this->DbInsertSucces) {
				echo "ja";
			}
			else {
				echo "nee";
			}
			echo "</td>";			
			echo "<td>";
			if ($this->FoundPdf) {
				echo "ja";
			}
			else {
				echo "nee";
			}

			$this->writeLogRegel();			
		}
		
		public function SetAantalFactImported()
		{
			$this->AantalFactImported = 0;	
			$sql = "SELECT count(*) as aantal
						FROM dbo.InvoiceArchiveLst
						WHERE DateInvoice = '".$this->DatumFactuur. "'";
			$result = sqlsrv_query($this->conn, $sql) or die( "$sql geeft: " . (print_r( sqlsrv_errors(), true)));
			$row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC);
			
			if($result){  
					$this->AantalFactImported = $row['aantal'];
			}
			else{  
				die("$sql geeft: " . (print_r( sqlsrv_errors(), true)));
			} 
			sqlsrv_free_stmt( $result);		
		}
		
		function writeLogRegel()
		{
			$this->setLogReden();
			
			fputcsv($this->logfile, array($this->KlantID, 
																		$this->FactuurNr, 
																		$this->TotaalInc, 	
																		$this->TotaalEx, 
																		$this->TotaalBTW,
																		$this->FactuurNr,
																		$this->DatumFactuur,
																		date("d-m-Y"),
																		date("H:i:s"),
																		$this->DbInsertSucces,
																		$this->FoundPdf,
																		$this->LogReden
																		), ";");
		}			



	
		public function EmailLog()
		{
			$file = 		$this->LogFileName;
			$subject = 'Factuur import log';
			
			$filename  = basename($file);
			$path      = $this->LogPath;
			$file      = $path . $filename;
			$file_size = filesize($file);
			$handle    = fopen($file, "r");
			$content   = fread($handle, $file_size);
			fclose($handle);

			$content = chunk_split(base64_encode($content));
			$uid     = md5(uniqid(time()));
			$name    = basename($file);

			$eol     = PHP_EOL;
			$subject = "Abillityfactuur in BO log";
			$message =  "
			<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>
				<title>Beaglevalley</title>
				<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
				
				<style type='text/css'> 
				body{
					font-familie	
					width:100% !important; 
					margin:0; 
					padding:0; 
					font-family: Arial, Helvetica, sans-serif;
				}  

			</style>
			</head>
				<body  style='width:100% !important; margin:0; padding:0;'>  
				In de bijlage de log van de afgelopen import van de facturen.
				<br />
				Start datum/tijd: {$this->StartDateTime}<br />
				Eind datum/tijd: {$this->EndDateTime}<br />
				FactuurDatum: {$this->DatumFactuur}<br />
				Aantal records ingelezen van invoice bestand: {$this->InvoiceCount}<br />
				Aantal facturen geimporteerd in de database: {$this->AantalFactImported}<br />
				<br />
				
				</body>
				</html>";       
			

			$from_name = "Kliksafe bedrijfsbureau";
			$from_mail = "bedrijfsbureau@kliksafe.nl";
			$replyto   = "bedrijfsbureau@kliksafe.nl";
			$mailto    = "jpeters@isp.kliksafe.nl, gevers@isp.kliksafe.nl";
			$header    = "From: " . $from_name . " <" . $from_mail . ">\n";
			$header .= "Reply-To: " . $replyto . "\n";
			$header .= "MIME-Version: 1.0\n";
			$header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\n\n";
			$emessage = "--" . $uid . "\n";
			$emessage .= "Content-type:text/html; charset=iso-8859-1\n";
			$emessage .= "Content-Transfer-Encoding: 7bit\n\n";
			$emessage .= $message . "\n\n";
			$emessage .= "--" . $uid . "\n";
			$emessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\n"; // use different content types here
			$emessage .= "Content-Transfer-Encoding: base64\n";
			$emessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\n\n";
			$emessage .= $content . "\n\n";
			$emessage .= "--" . $uid . "--";
			if (mail($mailto, $subject, $emessage, $header)){
					echo 'Mail succesvol verstuurd';
			}
			else{
				echo error_get_last();
			}
		}
	}	
?>