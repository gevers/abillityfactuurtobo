<?php
	include_once 'obj_Invoice.php'; 

	if (isset($_GET["insert"]) and ($_GET["insert"] == 1)){
		if (isset($_GET["InvoiceDate"]) and validateDate($_GET["InvoiceDate"])){
			$InvLst = new InvoiceLst();
			$InvLst->SetDatumFactuur($_GET["InvoiceDate"]);	
			
			$InvLst->SetAantalFacturenPerDatum($InvLst->GetDatumFactuur());
			if ($InvLst->GetAantalFacturenPerDatum() > 0 ){
				echo "Er zijn " .$InvLst->GetAantalFacturenPerDatum(). " facturen met deze invoiceDate gevonden. <br />";
				echo "De DateInvoice lijkt niet juist. Kies een andere datum!";
				exit;
			}
			else{
				echo "Er zijn op " .$InvLst->GetDatumFactuur(). " " .$InvLst->GetAantalFacturenPerDatum(). " facturen aanwezig";
			}
		}
		else{
			echo "Je hebt een verkeerde datum opgegeven. Datum formaat moet zijn 'yyyy-mm-dd'" ;
			exit;
		}
	}
	else{
		echo "insert not ok";
	}
	
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}	
	

		function ImportCSV2Array($filename)
		{
			$row = 0;
			$col = 0;
			$handle = @fopen($filename, "rb");
			if ($handle) 
			{
				while (($row = fgetcsv($handle, 4096, ";")) !== false) 
				{
					if (empty($fields)) 
					{
							$fields = $row;
							continue;
					}
					foreach ($row as $k=>$value) 
					{
						$results[$col][$fields[$k]] = $value;
					}
					$col++;
					unset($row);
				}
				if (!feof($handle)) 
				{
					echo "Error: unexpected fgets() failn";
				}
				fclose($handle);
			}
			return $results;
		}

			$filename = "doc/Testmaintype.csv";
			$csvArray = ImportCSV2Array($filename);
			echo "<table border=1 cellpadding=5 cellspacing=0>";
			echo "<tr>";
			echo "<td>CustomerNumber</td>";
			echo "<td>InvoiceNo</td>";
			echo "<td>Invoice total (inc VAT)</td>";
			echo "<td>Vat Amount 21.00</td>";
			echo "<td>Invoice total (exc VAT)</td>";
			echo "<td>Inserted in DB</td>";
			echo "<td>PDF aanwezig</td>";	
			echo "</tr>";
			foreach ($csvArray as $row)
			{	
					$InvLst->KlantID = $row['CustomerNumber'];
					$InvLst->FactuurNr = $row['InvoiceNo'];
					$tmp = $row['Invoice total (inc VAT)'];
					$InvLst->TotaalInc = str_replace(',', '.', $row['Invoice total (inc VAT)']);
					$InvLst->TotaalBTW = str_replace(',', '.', $row['Vat Amount 21.00']);
					$InvLst->TotaalEx = str_replace(',', '.', $row['Invoice total (exc VAT)']);		
					$factuur = "doc/PDF/" .$InvLst->FactuurNr. ".pdf";
					if (file_exists($factuur)){
						$InvLst->setFoundPdf('ja');
						$InvLst->setPdf($factuur);
					} else {
						$InvLst->setFoundPdf('nee');
					}
					$InvLst->saveToDb($InvLst);
					echo "</td>";
					echo "</tr>";
			}		
			echo "</table>";

		function loadPDF($filepath)
		{
				$handle = @fopen($filepath, 'rb');
				if ($handle)
				{
						$content = @fread($handle, filesize($filepath));
						$content = bin2hex($content);
						@fclose($handle);
						return "0x".$content;
				}	
		}	
	
?>