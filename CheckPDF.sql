use KliksafeStore_Prep;

SELECT lst.ID
      ,lst.InvoiceType
      ,lst.OrganizationID
      ,lst.DateCreated
      ,lst.DateInvoice
      ,lst.DateSended
      ,lst.DateExported
      ,lst.TotalCostSale
      ,lst.TotalGross
      ,lst.TotalVat
      ,lst.TotalInvoice
			,pdf.ID
			,pdf.InvoiceArchiveLstID
			,pdf.PdfText
  FROM InvoiceArchiveLst lst
	JOIN InvoiceArchivePdf pdf on lst.id = pdf.InvoiceArchiveLstID
	WHERE lst.InvoiceType = 'MM'
	
	DELETE FROM InvoiceArchivePdf
	WHERE InvoiceArchiveLstID >= 400477898

	SELECT *
	FROM InvoiceArchiveLst
	WHERE InvoiceType = 'MM'