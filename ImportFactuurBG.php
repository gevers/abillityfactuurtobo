<?php
	include_once 'obj_Import.php'; 

	if (isset($_GET["insert"]) and ($_GET["insert"] == 1))
	{
		if (isset($_GET["InvoiceDate"]) and validateDate($_GET["InvoiceDate"])){
			$InvLst = new InvoiceLst();
			$InvLst->SetDatumFactuur($_GET["InvoiceDate"]);	
			
			$InvLst->SetAantalFacturenPerDatum($InvLst->GetDatumFactuur());
			if ($InvLst->GetAantalFacturenPerDatum() > 0 ){
				die("Er zijn " .$InvLst->GetAantalFacturenPerDatum(). " facturen met deze invoiceDate gevonden. <br /> De DateInvoice lijkt niet juist. Kies een andere datum!");
			}
		}
		else{
			die("Je hebt een verkeerde datum opgegeven. Datum formaat moet zijn 'yyyy-mm-dd'");
		}
	}
	else{
		echo "insert not ok";
	}
	
	function validateDate($date, $format = 'Y-m-d')
	{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
	}		
	
?>

	<body>
			<div id="div1"></div>
		</p>

	</body>
</html>

<?php
	$cmd = 'C:\xampp\php\php.exe C:\xampp\htdocs\maandfactuur\ImportFactuur.php insert=1 InvoiceDate='.$InvLst->DatumFactuur;
	
	function execInBackground($cmd) 
	{ 
		$outputfile = 'c:/xampp/htdocs/maandfactuur/doc/log/log.html';
		$shellCmd = 'start /B cmd /c ' . escapeshellcmd($cmd) . ' ^>"'.$outputfile. '"';
		//$handle = popen("start /B ". $shellCmd . '>', "r");  
		$handle = popen($shellCmd, "r");  
		if ($handle === FALSE) {
			die("Unable to execute $cmd");
		}
		pclose($handle);			
	}

	execInBackground($cmd);
?>