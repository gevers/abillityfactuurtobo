<?php

	if(isset($_GET['del']))
	{

		$conn_array = array (
				"UID" => "ks-gerard",
				"PWD" => "SoTgRLi6LQR?OV!P2DVO",
				"Database" => "KliksafeStore",
		);
		$conn = sqlsrv_connect('212.121.123.58', $conn_array);
		
		if (!$conn){
			die( print_r( sqlsrv_errors(), true) );
		}
		$sql = "SELECT COUNT(*) as aantal FROM InvoiceArchiveLst WHERE InvoiceType = 'AM'";
		$result = sqlsrv_query($conn, $sql);
		if($result)
		{
			$deleteArray = array();
			while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
			
			echo "Er zijn {$row['aantal']} facturen die moeten verwijderd worden <br />";
			}
			sqlsrv_free_stmt( $result);
		}
		else  
		{  
			echo "Row verwijderen mislukt.\n";  
			die( print_r( sqlsrv_errors(), true));  
		} 
		// transation start
		if ( sqlsrv_begin_transaction( $conn ) === false ) {
				 die( print_r( sqlsrv_errors(), true ));
		}	

		$sqlDelPdf = "DELETE FROM InvoiceArchivePdf WHERE InvoiceArchiveLstID IN 
									(SELECT ID FROM InvoiceArchiveLst WHERE InvoiceType = 'AM')";
		$resultPdf = sqlsrv_query($conn, $sqlDelPdf);

		$sqlDelLst = "DELETE FROM InvoiceArchiveLst
							WHERE InvoiceType = 'AM'";
		$resultLst = sqlsrv_query($conn, $sqlDelLst);
	
		//transaction end
		if( $resultPdf && $sqlDelLst ) {
			sqlsrv_commit( $conn );
				echo "Facturen verwijderd.<br />";
		} else {
				sqlsrv_rollback( $conn );
				echo "Transaction rolled back. Facturen zijn niet verwijderd.<br />";

		}			
	}
?>	
